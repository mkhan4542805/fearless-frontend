function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="col-md-4 mb-4">
            <div class="card shadow">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                <small class="text-muted">Start Date: ${starts} | End Date: ${ends}</small>
            </div>
            </div>
        </div>
    `;
  }

  window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Network response was not ok');
      } else {
        let columnIndex = 0
        const data = await response.json();
        const row = document.querySelector('.row');
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const start_date = new Date(details.conference.starts);
            const end_date = new Date(details.conference.ends);

            const options = { year: "numeric", month: "long", day: "numeric" };
            const formatted_start_date = start_date.toLocaleDateString("en-US", options);
            const formatted_end_date = end_date.toLocaleDateString("en-US", options);

            const html = createCard(title, description, pictureUrl, formatted_start_date, formatted_end_date, location);
            const column = document.querySelector('#col-' + columnIndex);
            row.innerHTML += html;
            columnIndex += 1;
            if (columnIndex > 2) {
              columnIndex = 0
            }
          }
        }

      }
    } catch (e) {
        console.error('There was an error fetching the conference data:', e);
    }

  });
