window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }
    selectTag.classList.remove("d-none")

    const loadingSpinner = document.getElementById("loading-conference-spinner");
    loadingSpinner.classList.add("d-none");

    const createform = document.getElementById("create-attendee-form")
    createform.classList.add("d-none")

    const alertsuccess = document.getElementById("success-message")
    alertsuccess.classList.remove("d-none")
  });
